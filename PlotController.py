from main import GravitySimulator
from wx.lib.pubsub import Publisher
import PlotView

class PlotController():
    def __init__(self,app):
        self.model = GravitySimulator(50,100,1,10)
        
        self.XYView = PlotView.XYFrame(None, 'XY test window', self.model.bodies)
        #self.XZView = PlotView.XZFrame(None, 'XY test window', self.model.bodies)
        #self.YZView = PlotView.YZFrame(None, 'XY test window', self.model.bodies)
        
        Publisher.subscribe(self.bodiesChanged, "BODIES CHANGED")
        
        self.XYView.Show()
        #self.XZView.Show()
        #self.YZView.Show()
        
    def bodiesChanged(self, message):
        self.XYView.setBodies(message.data)
        self.XYView.Refresh()
