import unittest, wx, random
from Body import position
from Body import velocity
from Body import Body
from main import Newtonian
from main import GravitySimulator
import PlotView

class TestSequenceFunctions(unittest.TestCase):
    
    def setUp(self):
        position1 = position(0,0,0)
        position2 = position(1,1,1)
        
        self.body1 = Body(2,position1, velocity(0,0,0))
        self.body2 = Body(3,position2, velocity(0,0,0))
        
        self.assertEqual(2, self.body1.mass)
        self.assertEqual(3, self.body2.mass)
        
        self.bodies = {self.body1.position:self.body1, self.body2.position:self.body1}
        
        self.creatBodies(50,self.bodies)
            
    def creatBodies(self, numberOfBodies, bodies):
        """generate random bodies and add them to bodies dictionary"""
        random.seed()
        #this is stupid. Why can't I have a normal for loop
        for _ in range(numberOfBodies):
            #python is moronic
            self.tempPosition = position(random.randint(-250,250), random.randint(-250,250), random.randint(-250,250))
            
            self.tempVelocity = velocity(random.randint(-100,100),random.randint(-100,100),random.randint(-100,100))
            
            self.tempBody = Body(random.uniform(1,10), self.tempPosition, self.tempVelocity)
            self.bodies.update({self.tempPosition:self.tempBody})
        
    def test_ChangeMass(self):
        self.body1.mass = 5
        self.body2.mass = 20
        self.assertEqual(5, self.body1.mass)
        self.assertEqual(20, self.body2.mass)
        
    def test_MoveMass(self):
        position1 = position(5,6,3)
        self.body1.position = position1
        self.assertEqual(6, self.body1.position.y)
        
        position2 = position(8,3,4)
        self.body2.position = position2
        self.assertEqual(8, self.body2.position.x)
        
    def test_ChangeVelocity(self):
        positionForVelocity1 = position(1,0,-1)
        velocity1 = velocity(positionForVelocity1.x, positionForVelocity1.y, positionForVelocity1.z)
        self.body1.velocity = velocity1
        self.assertEqual(self.body1.velocity.x, positionForVelocity1.x)
        self.assertEqual(self.body1.velocity.y, positionForVelocity1.y)
        
    def test_Gravity(self):
        self.assertEqual(1, self.body2.position.x)
        gravity = Newtonian.gravity(self.body1, self.body2)
        self.assertEqual(.00000000013345180000000002, gravity)
        
    def test_MoveBody(self):
        aPosition = position(0,0,0)
        
        self.body = Body(2,aPosition, velocity(10,6,0))
        self.body.move()
        
        self.assertEqual(position(10,6,0), self.body.position)
        
    def test_BodyGravityInteraction(self):
        #not done
        position1 = position(0,0,0)
        position2 = position(10,10,10)
        
        self.body1 = Body(2,position1, velocity(0,0,0))
        self.body2 = Body(3,position2, velocity(0,0,0))
        
        #do the gravitying
 
        #figure out if they have moved and if they moved properly
        gravity = Newtonian.gravity(self.body1, self.body2)
        self.assertEqual(self.body1.position.x, self.position1.x+gravity)
        
    def test_Collision(self):
        pass
        
    def test_GraphBodies(self):
        app = wx.App(False)
        frame = PlotView.XYFrame(None, 'XY test window', self.bodies)
        frame.Show()
        app.MainLoop()
            
            
if __name__ == '__main__':
    unittest.main()
