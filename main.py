from math import pow, sqrt
import Body
from wx.lib.pubsub import Publisher as pub
import random

SIZE = 250

class GravitySimulator():
    def __init__(self, numberOfBodies, maxVelocity, minMass, maxMass):
        self.maxVelocity = maxVelocity
        self.minMass = minMass
        self.maxMass = maxMass
        self.bodies = {}
        self.creatBodies(numberOfBodies, self.bodies)
    
    def creatBodies(self, numberOfBodies, bodies):
        """generate random bodies and add them to bodies dictionary"""
        random.seed()
        for _ in range(numberOfBodies):
            tempPosition = Body.position(random.randint(-SIZE,SIZE), random.randint(-SIZE,SIZE), random.randint(-SIZE,SIZE))

            tempVelocity = Body.velocity(random.randint(-self.maxVelocity,self.maxVelocity),random.randint(-self.maxVelocity,self.maxVelocity),random.randint(-self.maxVelocity,self.maxVelocity))

            tempBody = Body.Body(random.uniform(self.minMass,self.maxMass), tempPosition, tempVelocity)
            self.bodies.update({tempPosition:tempBody})

    def moveBodies(self):
            
        #gravity
        for Body.position1, body1 in self.Bodies.iteritems():
                for Body.position2, body2 in self.Bodies.iteritems():
                    if (body1.position.x < body2.positon.x):
                        body2.velocity.x + Newtonian.gravity(body1, body2)
                    else:
                        body2.velocity.x - Newtonian.gravity(body1, body2)
                        
                    if (body1.position.y < body2.positon.y):
                        body2.velocity.y + Newtonian.gravity(body1, body2)
                    else:
                        body2.velocity.y - Newtonian.gravity(body1, body2)
                        
                    if (body1.position.z < body2.positon.z):
                        body2.velocity.z + Newtonian.gravity(body1, body2)
                    else:
                        body2.velocity.z - Newtonian.gravity(body1, body2)
                        
        #move the bodies                
        newBodies = {}
        for Body.position,body in self.bodies.iteritems():
            body.move()
            newBodies.update({body.position:body})
                        
            
        self.Bodies = newBodies
        
        pub.sendMessage("BODIES CHANGED", self.bodies)

class Newtonian:
    Gravity = .0000000000667259 #m^2/kg^2
    
    @staticmethod
    def gravity(mass1, mass2):
        distance = Newtonian.distance(mass1, mass2)
        return Newtonian.Gravity*((mass1.mass*mass2.mass)/pow(distance,2))
    
    @staticmethod
    def distance(mass1, mass2):
        p1 = mass1.position.x
        p2 = mass1.position.y
        p3 = mass1.position.z
        
        q1 = mass2.position.x
        q2 = mass2.position.y
        q3 = mass2.position.z
        return sqrt( pow(p1-q1,2) + pow(p2-q2,2) + pow(p3-q3,2) )
