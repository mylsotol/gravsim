class cartesianCoordinates():
    def __init__(self, x, y, z):
        self.x=x
        self.y=y
        self.z=z

class position(cartesianCoordinates):
    """position of the body"""

class velocity(cartesianCoordinates):
    """
    direction is a position, only the signs are important and magnitude is speed in that direction
    """


class Body:  
    def __init__(self, mass=None, position=None, velocity=None):
        """mass is in kg"""
        self.mass = mass
        self.position = position
        self.velocity = velocity
        
        #momentum
        self.momentumX = self.mass * self.velocity.x
        self.momentumY = self.mass * self.velocity.y
        self.momentumZ = self.mass * self.velocity.z

    def move(self):
        self.position.x += self.velocity.x
        self.position.y += self.velocity.y
        self.position.z += self.velocity.z
