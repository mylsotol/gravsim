import wx

SIZE = 500

class XYFrame(wx.Frame):
    def __init__(self,parent,title, bodies):
        #self.count = 0
        wx.Frame.__init__(self, parent, title=title, size=(SIZE,SIZE))
        self.bodies = bodies
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        
    def setBodies(self, bodies):
        self.bodies = bodies
        
    def OnPaint(self, event=None):
        #self.count +=1
        #self.SetTitle(str(self.count))
        dc = wx.PaintDC(self)
        dc.Clear()
        dc.SetPen(wx.Pen(wx.BLACK, 6))
        dc.SetBrush(wx.Brush(wx.BLACK))
        self.PaintBodies(dc)
     
    def PaintBodies(self,dc):   
        for position, body in self.bodies.iteritems():
            dc.DrawCircle(SIZE/2 + position.x,SIZE/2 + position.y, body.mass)
        
    def OnExit(self,e):
        self.Close(True)  # Close the frame.
        
class XZFrame(XYFrame):
    def PaintBodies(self,dc):   
        for position, body in self.bodies.iteritems():
            dc.DrawCircle(SIZE/2 + position.x,SIZE/2 + position.z, body.mass)

class YZFrame(XYFrame):
    def PaintBodies(self,dc):   
        for position, body in self.bodies.iteritems():
            dc.DrawCircle(SIZE/2 + position.y,SIZE/2 + position.z, body.mass)
